#!/usr/bin/env bash

src_dir=./proto
dist_dir_go=./proto-gen/go

# Find all directories containing at least one prototfile.
# Based on: https://buf.build/docs/migration-prototool#prototool-generate.
for dir in $(find ${src_dir} -name '*.proto' -print0 | xargs -0 -n1 dirname | sort | uniq); do
	files=$(find "${dir}" -name '*.proto')

	# Generate all files with protoc-gen-go.
	protoc \
		-I ./proto \
		--go_out=paths=source_relative:${dist_dir_go} \
		--go-grpc_out=paths=source_relative:${dist_dir_go} \
		${files}
done
